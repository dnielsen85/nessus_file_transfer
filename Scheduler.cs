﻿using System.IO;
using System.ServiceProcess;
using System.Threading;
using System.Timers;

namespace NessusService
{
    public partial class Scheduler : ServiceBase
    {
        private System.Timers.Timer timer1 = null;

        public Scheduler()
        {
            InitializeComponent();
        }


        protected override void OnStart(string[] args)
        {


            ProgramFunctions.CreateDirectories();
            ReadMeDoc.README();
            Logging.TailLogPowerShellScript();

            timer1 = new System.Timers.Timer();
            timer1.Interval = 60000;  // <-- Poll every X Min * x
            Logging.WriteLog("*************** Starting Service****************");
            Logging.WriteLog("Updates being checked every " + ((timer1.Interval / 1000) / 60) + " Minute(s)");
            
            timer1.Elapsed += new ElapsedEventHandler(timer1_tick);
            timer1.Enabled = true;

            timer1.AutoReset = false;


        }

        private void timer1_tick(object sender, ElapsedEventArgs e)
        {

            try
            {
                

                    ProcessTimerEvent(lockObject);
                
            }

            finally
            {

                timer1.Enabled = true;
                DirectoryInfo getUpdates = new DirectoryInfo(ProgramDirectories.NSOC);
                FileInfo[] Files = getUpdates.GetFiles("*.*");

                if (Files.Length == 0)
                {

                    ProgramFunctions.Cleanup();
                    Logging.WriteLog("Finishing updates");

                    

                }


            }

        }
        object lockObject = new object();
        private void ProcessTimerEvent(object state)
        {
            if (Monitor.TryEnter(lockObject))
            {
                try
                {
                
                    Program.Main();
                    
                }
                finally
                {
                    Monitor.Exit(lockObject);
                }
            }
        }


        protected override void OnStop()
        {
            Logging.WriteLog("*************** Stopping Service");
            this.ExitCode = 0;
            base.OnStop();
        }
    }
}





